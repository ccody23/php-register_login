<?php
//connect to database
include 'config/db.php';

class User {
  public $db;

  //user register
  public function registerUser($firstname, $lastname, $email, $password){
    $conn = db();
    //check if email already exists in database
    $email_check_query = mysqli_query($conn, "SELECT * FROM users WHERE email = '{$email}' ");
    $rowCount = mysqli_num_rows($email_check_query);

    if($rowCount == 0)
    {
      $_fname = mysqli_real_escape_string($conn, $firstname);
      $_lname = mysqli_real_escape_string($conn, $lastname);
      $_email = mysqli_real_escape_string($conn, $email);
      $_password = mysqli_real_escape_string($conn, $password);

      //hash password
      $options = [
        'cost' => 12,
      ];

      $password_hash = password_hash($_password, PASSWORD_BCRYPT, $options);

      //insert data into the database

      //database query
      $sql = "INSERT INTO users (firstname, lastname, email, userpassword, date_time) VALUES ('{$_fname}', '{$_lname}', '{$_email}', '{$password_hash}', now())";

      //Create mysql query
      $sqlQuery = mysqli_query($conn, $sql);


      return $sqlQuery;
    }
    else{
      return false;
    }

  }

  //user login
  public function loginUser($email, $password){
    $conn = db();
    //sanitize email and password
    $_email = mysqli_real_escape_string($conn, $email);
    $_password = mysqli_real_escape_string($conn, $password);

    //check if email exists
    $sql = "SELECT * From users WHERE email = '{$_email}' ";
    $query = mysqli_query($conn, $sql);
    $rowCount = mysqli_num_rows($query);

    // If query fails, show the reason
    if(!$query){
      die("SQL query failed: " . mysqli_error($conn));
    }

    if($rowCount == 1){
      // Fetch user data and store in php session
      while($row = mysqli_fetch_array($query)) {
        $id            = $row['user_id'];
        $firstname     = $row['firstname'];
        $lastname      = $row['lastname'];
        $email         = $row['email'];
        $pass_word     = $row['userpassword'];
      }

      //verify password
      if(password_verify($password, $pass_word )){

        $_SESSION["loggedIn"] = true;
        $_SESSION['id'] = $id;
        $_SESSION['firstname'] = $firstname;
        $_SESSION['lastname'] = $lastname;
        $_SESSION['email'] = $email;
        return true;
      }
      else{
        return false;
      }
    }

  }

  public function logoutUser(){
    $_SESSION["loggedIn"] = false;
	  session_destroy();
  }

}


?>
