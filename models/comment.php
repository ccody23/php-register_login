<?php
//connect to database
include '../config/db.php';

class Comment{
  public $db;

  public function addComment($comment_text, $user_id, $post_id){
    $conn = db();

    $_comment_text = mysqli_real_escape_string($conn, $comment_text);
    $_user_id = mysqli_real_escape_string($conn, $user_id);
    $_post_id = mysqli_real_escape_string($conn, $post_id);
    $date = date("Y-m-d H:i:s");

    $sql = "INSERT into comments (comment_text, comment_date, user_id, post_id) values('{$_comment_text}', '{$date}', '{$_user_id}', '{$_post_id}')";

    //Create mysql query
    $sqlQuery = mysqli_query($conn, $sql);


    if(!$sqlQuery){
      echo("Error description: " . mysqli_error($conn));
    }
    else{
      return $sqlQuery;
    }
  }
}

?>
