<?php
session_start();
if ($_SESSION["loggedIn"] == false){
  header("Location: https://localhost/php-register_login/index.php");
}
?>


<!-- Header -->
<?php include('header.php'); ?>


<?php
include 'config/db.php';
$conn = db();
  /* Get All Posts */


  $sql = "SELECT posts.post_id, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname FROM posts INNER JOIN users ON posts.user_id=users.user_id order by post_id desc;";
  $result = $conn->query($sql);

?>

<!-- Modal -->
<div class="modal fade" id="empModal" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="post-content">
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div style="width:100%; display:flex; justify-content:center;">
  <div style="margin-top:50px; width:50%; display:flex; justify-content:center; display:flex; flex-direction:column">
    <div id="post_err"></div>
    <div class="form-group">
      <label for="post_title">Title</label>
      <input type="text" class="form-control" value="" id="post-title" placeholder="Enter title">
      <div id="post_title_err"></div>
    </div>
    <div class="form-group">
      <label for="post_message">Message:</label>
      <textarea class="form-control" rows="5" id="post-message"  value="" placeholder="Enter message"></textarea>
      <div id="post_message_err"></div>
    </div>

      <button onclick="addPosts(<?php echo $_SESSION["id"]?>)" class="btn btn-primary">Post</button>
  </div>
</div>

<div class="main">
  <div id="posts" class="posts">
  </div>
</div>

<?php include('footer.php'); ?>
