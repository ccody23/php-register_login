window.onload = function() {
  //disable new animation post when the page first loads
  loadPosts();
};

function fadeOutError(){
  document.getElementById("mainError").classList.remove("animate__fadeInDown");
  document.getElementById("mainError").classList.add("animate__fadeOutUp");
  setTimeout(function(){
    document.getElementById("mainError").remove();
  }, 150);
}

function addPosts(id){
  var title = document.getElementById("post-title");
  var message = document.getElementById("post-message");

  //remove all error messages
  document.getElementById("post_err").innerHTML = "";
  document.getElementById("post_title_err").innerHTML = "";
  document.getElementById("post_message_err").innerHTML = "";

  console.log(title.value);
  if(title.value === "" && message.value === ""){
    console.log("Title and message are empty");
    document.getElementById("post_err").innerHTML = "<div id='mainError' class='alert alert-danger animate__animated animate__fadeInDown animate__faster' role='alert'>Please enter a value for title and message<button onclick='fadeOutError()' type='button' class='close' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
  }
  else if(title.value === ""){
    document.getElementById("post_title_err").innerHTML = "<span class='err-msg'>Please enter a value for title and message</span>";
  }
  else if(message.value === ""){
    document.getElementById("post_message_err").innerHTML = "<span class='err-msg'>Please enter a value for title and message</span>";
  }
  else{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        //add validation for new post
        //reset form after successfull post
        //add animation for new post
        var posts = document.getElementsByClassName('post');
        posts[0].classList.add("animate__fadeInDown");
        loadPosts();
      }
    };
    xhttp.open("GET", "controllers/post-message.php?id=" + id + "&title=" + title.value + "&message=" + message.value, true);
    title.value = "";
    message.value = "";
    xhttp.send();
    console.log("success");
  }
}

function loadPosts(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      $('#posts').html(this.responseText);

    }
  };
  xhttp.open("GET", "controllers/updateposts.php", true);
  xhttp.send();
}

function loadComments(post_id){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){

      // Add response in Modal body
      $('.post-content').html(this.responseText);
      // Display Modal
      $('#empModal').modal('show');

    }
  };
  xhttp.open("GET", "controllers/showmodal.php?post=" + post_id, true);
  xhttp.send();
}

function updateComments(post_id){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){

      // Add response in Modal body
      console.log(Number(post_id));
      console.log(this.responseText);
      $('#comment-section').html(this.responseText);

    }
  };
  xhttp.open("GET", "controllers/updatecomments.php?post=" + post_id, true);
  xhttp.send();
}

function showNewComment(id, post_id){
  var comment = document.getElementById("comment-text").value;
  if(comment === ""){
    console.log("please enter a comment before submitting");
    document.getElementById("comment_err").innerHTML = "<div id='commentError' class='alert alert-danger animate__animated animate__fadeInDown animate__faster' role='alert'>Please enter a comment<button onclick='fadeOutError()' type='button' class='close' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
  }
  else{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){

        // Add response in Modal body
        var jsonstring = JSON.parse(this.responseText);
        console.log(jsonstring["post_id"]);

        if(jsonstring["message"] == "success"){
          console.log(parseInt(jsonstring["user_id"]));
          updateComments(jsonstring["post_id"]);
        }
        else{
          console.log("error");
        }
      }
    };
    xhttp.open("GET", "controllers/post-comment.php?user_id=" + id + "&post_id=" + post_id + "&comment_text=" + comment, true);
    xhttp.send();
  }
}
