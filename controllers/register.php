<?php

session_start();
	//n
include 'models/user.php';

//Error and success messages
global $success_msg, $email_exist, $fnameErr, $lnameErr, $emailErr, $passwordErr, $cpasswordErr;
global $fnameEmptyErr, $lnameEmptyErr, $emailEmptyErr, $passwordEmptyErr, $cpasswordEmptyErr;

$fname = $lname = $email = $password = $cpassword = "";

if(isset($_POST['submit'])) {
  $fname = $_POST["fname"];
  $lname = $_POST["lname"];
  $email = $_POST["email"];
  $password = $_POST["password"];
  $cpassword = $_POST["cpassword"];

  //php validation
  //check if form entries are not empty
  if(!empty($fname) && !empty($lname) && !empty($email) && !empty($password) && !empty($cpassword)){

      if($password !== $cpassword)
      {
        $cpasswordErr = "<span class='err-msg'>Passwords don't match</span>";
      }
      if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $emailErr = "<span class='err-msg'>Please enter valid email</span>";
      }
      if(strlen($password) < 8 || ctype_lower($password) || !preg_match('~[0-9]+~', $password)){
        $passwordErr = "<span class='err-msg'>Please enter valid password</span>";
      }
      else{

        $user = new User;

        $register = $user->registerUser($fname, $lname, $email, $password);

        if($register)
        {
          //registration success

          //clear form data
          $fname = $lname = $email = $password = $cpassword = "";

          //show user that account has been created successfully
          $success_msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Your account was created successfully! <a href="index.php">Click here to log into your account</a>
          </div>';

        }
        else{
          //registration failed
          $email_exist = '
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            User with that email already exists!
          </div>';
        }

      }


  }
  else{
    //give error message for empty inputs
    if(empty($fname)){
      $fnameEmptyErr = "<span class='err-msg'>First name is a required field</span>";
    }
    if(empty($lname)){
      $lnameEmptyErr = "<span class='err-msg'>Last name is a required field</span>";
    }
    if(empty($email)){
      $emailEmptyErr = "<span class='err-msg'>Email is a required field</span>";
    }
    if(empty($password)){
      $passwordEmptyErr = "<span class='err-msg'>Password is a required field</span>";
    }
    if(empty($cpassword)){
      $cpasswordEmptyErr = "<span class='err-msg'>Confirm Password is a required field</span>";
    }
  }

}

?>
