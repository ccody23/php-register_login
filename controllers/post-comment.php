<?php

include '../models/comment.php';

  $user_id = $_REQUEST["user_id"];
  $post_id = $_REQUEST["post_id"];
  $comment_text = $_REQUEST["comment_text"];


  $comment = new Comment();

  $addComment = $comment->addComment($comment_text, $user_id, $post_id);

  if($addComment){
    $output = array(
    'message' => "success",
    'user_id' => $user_id,
    'post_id' => $post_id
    );
    echo json_encode($output);
  }
  else{
    echo "error";
  }


?>
