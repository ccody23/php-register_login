<?php
session_start();
//connect to database
include 'models/user.php';
$conn = db();


if(isset($_POST['login'])) {
  $email = $_POST["email"];
  $password = $_POST["password"];
  $user = new User();
  $login = $user->loginUser($email, $password);

  if($login){
    header("Location: ./dashboard.php");
  }
  else{
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Error</strong> The Password you entered is incorrect!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
  </div>';
  }

}

?>
