<?php
session_start();
include '../config/db.php';


$conn = db();
$id = $_REQUEST["post"];

$sql = "SELECT posts.post_id, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname FROM posts INNER JOIN users ON posts.user_id=users.user_id where post_id = $id order by post_id desc";
$result = mysqli_query($conn,$sql);

$commentsql = "SELECT posts.post_id, comments.comment_text, comments.comment_date, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname FROM posts INNER JOIN users ON posts.user_id=users.user_id INNER JOIN comments ON posts.post_id = comments.post_id WHERE posts.post_id = $id order by post_id desc";
$secondresult = mysqli_query($conn, $commentsql);

while($row = mysqli_fetch_array($result)){
  $firstname = $row['firstname'];
  $lastname = $row['lastname'];
  $post_title = $row["post_title"];
  $post_text = $row["post_text"];
  $date_posted = $row["date_posted"];
  $likes = $row["likes"];
  $dislikes = $row["dislikes"];
  $user_id = $row["user_id"];

  $response = " ";
  $response .= "<div id='comment_err'></div>";
  $response .= "<div class='modal-header'>";
  $response .= "<h4 class='modal-title'>" . $firstname . " " . $lastname . " <strong>" . $post_title . "</strong></h4>";
  $response .= "<button type='button' class='close' data-dismiss='modal'>&times;</button>";
  $response .= "</div>";
  $response .= "<div class='modal-body'>";
  $response .= "<h2>". $post_text ."</h2>";
  $response .= "<div class='post_footer'>";
  $response .= "<div class='post_date'>". $date_posted ."</div>";
  $response .= "<div class='post_like'><i class='fa fa-thumbs-up'></i> ".$likes."</div>";
  $response .= "<div class='post_dislike'><i class='fa fa-thumbs-down'></i>". $dislikes ."</div>";
  $response .= "</div>";
  $response .= "<hr>";
  $response .= "<textarea id='comment-text' class='form-control' placeholder='Write a comment'></textarea>";
  $response .= "<input type='hidden' name='post_id' value='$id' />";
  $response .= "<input type='hidden' name='user_id' value='$user_id' />";
  $response .= "<br/>";
  $response .= "<button onclick='showNewComment(".$user_id.", ".$id.")' id='addcomment' name='comment-post' class='btn btn-primary'>Add Comment</button>";
  $response .= "</div>";
}
$response .= "<div id='comment-section'>";
while($row = mysqli_fetch_array($secondresult)){
  $comment_text = $row["comment_text"];
  $firstname = $row["firstname"];
  $lastname = $row["lastname"];
  $comment_date = $row["comment_date"];
  $response .= "<div class='comment'>";
  $response .= "<p><strong>". $row['firstname'] . " " . $row['lastname']. "</strong>  posted on " . $comment_date . "</p>";
  $response .= "<p>". $comment_text ."</p>";
  $response .= "</div>";
}
  $response .= "</div>";

echo $response;
?>
