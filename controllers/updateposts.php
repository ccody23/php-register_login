<?php
session_start();
include '../config/db.php';


$conn = db();

$sql = "SELECT posts.post_id, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname FROM posts INNER JOIN users ON posts.user_id=users.user_id order by post_id desc;";
$result = $conn->query($sql);
$response = " ";
while($row = mysqli_fetch_array($result)){
  $response .= "<div class='post'>";
  $response .= "<div class='post_header'>";
  $response .= "<div class='post_user'>".$row["firstname"]." ".$row["lastname"]."</div>";
  $response .= "<div class='post_title'><strong>".$row["post_title"]."</strong></div>";
  $response .= "</div>";
  $response .= "<div class='post_text'>".$row["post_text"]."</div>";
  $response .= "<div class='post_footer'>";
  $response .= "<div class='post_date'>".$row["date_posted"]."</div>";
  $response .= "<div class='post_like'><i class='fa fa-thumbs-up'>".$row["likes"]."</i></div>";
  $response .= "<div class='post_dislike'><i class='fa fa-thumbs-down'>".$row["dislikes"]."</i></div>";
  $response .= "</div>";
  $response .= "<button onclick='loadComments(".$row["post_id"].")' style='width:200px' id=".$row["post_id"]." class='commentbtn btn btn-primary'>View Comments</button>";
  $response .= "</div>";

}

echo $response;

?>
