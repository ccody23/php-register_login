

<!-- Header -->
<?php include('header.php'); ?>

<!-- Login Script -->
<?php include('./controllers/login.php') ?>

<div class="container">
  <h1 class="title">Login<h1>

  <div class="jumbotron">
    <form action="" method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="text" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
      </div>
      <div class="form-group">
        <label for="password1">Password</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
      </div>
      <span id="passmsg"></span>
      <br/>

        <button type="submit" name="login" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>

<!-- Footer -->
<?php include('footer.php'); ?>
