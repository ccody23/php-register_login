<?php
$loggedIn = false;
if(session_id() != ''){
  $loggedIn = true;
  $user = $_SESSION["firstname"] . ' ' . $_SESSION["lastname"];
}
else{
  $loggedIn = false;
}
?>

<!DOCTYPE html>
<html>
  <head>
    <!-- required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <script src="https://use.fontawesome.com/8976c3bbca.js"></script>
    <title>PHP User Registration & Login</title>

  </head>
  <body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">PHP User Registration & Login</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Register</a>
      </li>
    </ul>
    <span class="navbar-text">
      <?php
      if ($loggedIn === false){
        echo '<a href="https://localhost/php-register_login/signup.php" class="btn btn-secondary" role="button" aria-pressed="true">Register</a>';
        echo ' ';
        echo '<a href="https://localhost/php-register_login/index.php" class="btn btn-secondary" role="button" aria-pressed="true">Login</a>';
      }
      else if($loggedIn === true){
        echo '<span style="margin-right:10px; font-size:20px">Hi, '. $user .'</span> ';
        echo '<a href="https://localhost/php-register_login/logout.php" class="btn btn-secondary" role="button" aria-pressed="true">Logout</a>';
      }
      ?>
    </span>
  </div>
</nav>
