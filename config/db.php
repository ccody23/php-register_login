<?php

define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'php_auth');

if(!isset($_SESSION)) {
  session_start();
  //create logged in session variable
  $_SESSION["loggedIn"] = false;
  $_SESSION["id"];
}

function db () {
    static $conn;
    if ($conn===NULL){
        $conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    }
    return $conn;
}


?>
