CREATE DATABASE php_auth;

CREATE DATABASE tutorial;


USE php_auth;

DROP TABLE comments;
DROP TABLE posts;
DROP TABLE users;


CREATE TABLE users(
	user_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(100) NOT NULL,
	lastname VARCHAR(100) NOT NULL,
	email VARCHAR(50) NOT NULL,
	userpassword VARCHAR(255) NOT NULL,
	date_time DATE NOT NULL 
);

CREATE TABLE posts(
	post_id INT(11) NOT NULL AUTO_INCREMENT,
	post_title VARCHAR(200) NOT NULL,
	post_text VARCHAR(200) NOT NULL,
	likes INT NOT NULL,
	dislikes INT NOT NULL,
	date_posted DATE NOT NULL,
	user_id INT,
	PRIMARY KEY (post_id),
	FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE comments(
	comment_id INT(11) NOT NULL AUTO_INCREMENT,
	comment_text VARCHAR(200) NOT NULL,
	comment_date DATE NOT NULL,
	user_id INT,
	post_id INT,
	PRIMARY KEY(comment_id),
	FOREIGN KEY(post_id) REFERENCES posts(post_id),
	FOREIGN KEY (user_id) REFERENCES users(user_id)
);

/* Insert fake posts */
INSERT INTO posts(post_title, post_text, likes, dislikes, date_posted, user_id)
VALUES ('first title', 'This is a first post', 20, 10, NOW(), 1);

INSERT INTO posts(post_title, post_text, likes, dislikes, date_posted, user_id)
VALUES ('second title', 'This is a second post', 20, 10, NOW(), 1);

INSERT INTO posts(post_title, post_text, likes, dislikes, date_posted, user_id)
VALUES ('third title', 'This is a third post', 20, 10, NOW(), 1);

INSERT INTO posts(post_title, post_text, likes, dislikes, date_posted, user_id)
VALUES ('third title', 'This is a thfdfsdird post', 20, 10, NOW(), 1);

/* Insert a comment to the first post */
INSERT INTO comments(comment_text, user_id, post_id)
VALUES('This is a comment for the first post', 1, 2);

INSERT INTO comments(comment_text, comment_date, user_id, post_id)
VALUES('This is another comment fsfsdf', NOW(), 1, 2);


SELECT userpassword FROM users WHERE email = "codyconnor21@gmail.com";

SELECT firstname from users where user_id = 1;

DELETE FROM users WHERE firstname = "Cody";
SELECT * FROM users;
SELECT * FROM posts ORDER BY post_id desc;
SELECT * FROM comments WHERE post_id = 2;
SELECT * FROM comments;
/* select post and comments for post */

SELECT comment_text, user_id, firstname, lastname, email, date_posted, posts.post_id, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id FROM posts
INNER comments
INNER JOIN posts ON 

SELECT posts.post_id, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname
FROM posts
INNER JOIN users ON posts.user_id=users.user_id;


SELECT posts.post_id, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname
FROM comments
INNER JOIN users ON posts.user_id=users.user_id;
INNER JOIN posts ON comments.post_id = posts.post_id;

SELECT comment_text, post_title FROM comments
INNER JOIN posts ON comments.post_id = comments.post_id;

SELECT firstname, comment_text, post_id FROM comments
INNER JOIN users ON comments.user_id = users.user_id WHERE comments.post_id = 2;

SELECT posts.post_id, comments.comment_text, comments.comment_date, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname FROM posts 
INNER JOIN users ON posts.user_id=users.user_id 
INNER JOIN comments ON posts.post_id = comments.post_id WHERE posts.post_id = 2
order by post_id DESC;

SELECT posts.post_id, comments.comment_id, comments.comment_text, comments.comment_date, posts.post_title, posts.post_text, posts.likes, posts.dislikes, posts.date_posted, posts.user_id, users.firstname, users.lastname FROM posts INNER JOIN users ON posts.user_id=users.user_id INNER JOIN comments ON posts.post_id = comments.post_id WHERE posts.post_id = 15 order by comments.comment_id desc