<!-- Header -->
<?php include('header.php'); ?>

<!-- Login Script -->
<?php include('./controllers/register.php') ?>

<div class="container">
  <h1 class="title">Register<h1>
  <?php echo $success_msg; ?>
  <?php echo $email_exist; ?>
  <div class="jumbotron">
    <form action="" method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">First Name</label>
        <input type="text" name="fname" value="<?php echo $fname ?>" class="form-control" id="fname" placeholder="Enter your first name">
        <?php echo $fnameEmptyErr; ?>
        <?php echo $fnameErr; ?>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Last Name</label>
        <input type="text" name="lname" value="<?php echo $lname ?>" class="form-control" id="lname" placeholder="Enter your last name">
        <?php echo $lnameEmptyErr; ?>
        <?php echo $lnameErr; ?>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="text" name="email" value="<?php echo $email ?>" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
        <?php echo $emailEmptyErr; ?>
        <?php echo $emailErr; ?>
      </div>
      <div class="form-group">
        <label for="password1">Password</label>
        <input type="password" name="password" value="<?php echo $password ?>" class="form-control" id="password" placeholder="Password" oninput="checkPasswords()">
        <span style="font-size:15px">Password must be at least 8 characters, contain one capital letter and a number</span>
        <br/>
        <?php echo $passwordEmptyErr; ?>
        <?php echo $passwordErr; ?>
      </div>
      <div class="form-group">
        <label for="password2">Confirm Password</label>
        <input type="password" name="cpassword" value="<?php echo $cpassword ?>" class="form-control" id="cpassword" placeholder="Confirm Password" oninput="checkPasswords()">
        <?php echo $cpasswordEmptyErr; ?>
        <?php echo $cpasswordErr; ?>
      </div>
      <span id="passmsg"></span>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="show-pass" onchange="showPasswords()">
        <label class="form-check-label" for="invalidCheck2">
          Show Passwords
        </label>
      </div>
      <br/>

        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>

<script>

/* show user that both passwords match*/
function checkPasswords(){
  var pass1 = document.getElementById("password");
  var pass2 = document.getElementById("cpassword");

  if(pass1.value === "" && pass2.value === ""){
    document.getElementById("passmsg").innerHTML = "";
  }
  else if(pass1.value === pass2.value){
    document.getElementById("passmsg").innerHTML = "<p style='color:green'>Passwords Match <i class='fa fa-check'></i></p>";
  }
  else if(pass1.value !== pass2.value){
    document.getElementById("passmsg").innerHTML = "<p style='color:red'>Passwords Don't Match <i class='fa fa-times'></i></p>";
  }
}

//show Passwords
function showPasswords(){
  if(document.getElementById("show-pass").checked == true){
    document.getElementById("password").type = "text";
    document.getElementById("cpassword").type = "text";
  }
  else if(document.getElementById("show-pass").checked == false){
    document.getElementById("password").type = "password";
    document.getElementById("cpassword").type = "password";
  }
}
</script>

<!-- Header -->
<?php include('footer.php'); ?>
